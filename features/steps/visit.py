from behave import given, when, then
from selenium.common.exceptions import NoSuchElementException
from pet_app.models import Vet, Pet


#LAB 7 BY KARL KUUSIK AND JAKOB JUURIK

@when("user registers a visit with valid input")
def step_impl(context):
    Vet.objects.create(name="Janice", age="25", education="Uni")
    br = context.browser
    br.find_element_by_link_text('Vets').click()


    try:
        br.find_element_by_name('book').click()
    except NoSuchElementException:
        br.find_element_by_id('dropdownMenuButton').click()
        br.find_element_by_link_text('Petty').click()

    assert 'visit' in br.current_url, \
        'Starting the visit was not successful.'
    br.find_element_by_name('submit').click()


@then("the visit is registered")
def step_impl(context):
    br = context.browser
    assert len(br.find_elements_by_css_selector('.alert.alert-success')) == 1, \
        'Unsucessful visit save.'