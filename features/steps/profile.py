from behave import given, when, then
from pet_app.test.factories.user import UserFactory
from selenium.webdriver.support.ui import Select
from pet_app.models import Vet, Pet


#LAB 7 BY KARL KUUSIK AND JAKOB JUURIK

@given('a registered user')
def step_impl(context):
    # Creates user with given credentials and saves it to database.
    # Creates user with given credentials and saves it to database.
    u = UserFactory(username='foo', email='foo@example.com')
    u.set_password('bar')
    u.save()

    br = context.browser
    br.get(context.base_url + '/login/')

    # locate elements by name, id, tag, css class, etc
    br.find_element_by_name('username').send_keys('foo')
    br.find_element_by_name('password').send_keys('bar')
    br.find_element_by_name('submit').click()


@given('a registered user with a pet {pet}')
def step_impl(context, pet):
    # Creates user with given credentials and saves it to database.
    global p
    u = UserFactory(username='foo', email='foo@example.com')
    u.set_password('bar')
    u.save()

    br = context.browser
    br.get(context.base_url + '/login/')

    # locate elements by name, id, tag, css class, etc
    br.find_element_by_name('username').send_keys('foo')
    br.find_element_by_name('password').send_keys('bar')
    br.find_element_by_name('submit').click()

    br.find_element_by_link_text('Profile').click()
    br.find_element_by_name('name').send_keys('Petty')
    select = Select(br.find_element_by_name('pet_type'))
    select.select_by_visible_text(pet)
    br.find_element_by_name('submit-pet').click()
    assert len(br.find_elements_by_css_selector('.alert.alert-success')) == 1, \
        'Unsucessful pet save.'


@given('a registered user with multiple pets and a pet {pet}')
def step_impl(context, pet):
    # Creates user with given credentials and saves it to database.
    global p
    u = UserFactory(username='foo', email='foo@example.com')
    u.set_password('bar')
    u.save()

    br = context.browser
    br.get(context.base_url + '/login/')

    # locate elements by name, id, tag, css class, etc
    br.find_element_by_name('username').send_keys('foo')
    br.find_element_by_name('password').send_keys('bar')
    br.find_element_by_name('submit').click()

    br.find_element_by_link_text('Profile').click()
    br.find_element_by_name('name').send_keys('Alty')
    br.find_element_by_name('submit-pet').click()
    br.find_element_by_name('name').send_keys('Petty')
    select = Select(br.find_element_by_name('pet_type'))
    select.select_by_visible_text(pet)
    br.find_element_by_name('submit-pet').click()
    assert len(br.find_elements_by_css_selector('.alert.alert-success')) == 1, \
        'Unsucessful pet save.'


@when('user books a visit for the pet')
def step_impl(context):
    br = context.browser

    br.find_element_by_link_text('Profile').click()
    br.find_element_by_name('book-visit').click()

    assert br.current_url.endswith('/vets/'), \
        'User is directed to the wrong url'


@when('user adds new First name, Last name, Email')
def step_impl(context):
    br = context.browser

    br.find_element_by_link_text('Profile').click()

    br.find_element_by_name('first_name').send_keys('Testname')
    br.find_element_by_name('last_name').send_keys('Testname')
    br.find_element_by_name('email').send_keys('generic@gmail.com')
    br.find_element_by_name('submit-user').click()


@then('data is updated')
def step_impl(context):
    br = context.browser

    name = br.find_element_by_name('first_name').get_attribute("value")
    last = br.find_element_by_name('last_name').get_attribute("value")
    email = br.find_element_by_name('email').get_attribute("value")

    assert name == 'Testname', \
        'Name is incorrect: '+name+'.'
    assert last == 'Testname', \
        'Last Name is incorrect.'+last+'.'
    assert email == 'generic@gmail.com', \
        'Email is incorrect.'
