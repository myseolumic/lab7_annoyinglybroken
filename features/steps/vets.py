from behave import given, when, then
from pet_app.models import Vet, Pet


#LAB 7 BY KARL KUUSIK AND JAKOB JUURIK

@given('there are vets in database')
def _add_vets_to_db(context):
    for row in context.table.rows:
        Vet.objects.create(name=row['name'], age=row['age'], education=row['education'])


@when('opening vets page')
def step_impl(context):
    br = context.browser
    br.get(context.base_url)

    br.find_element_by_link_text('Vets').click()


@then('all {x} vets are visible on the page')
def step_impl(context, x):
    br = context.browser

    num = len(br.find_elements_by_css_selector('.table tr'))

    assert num == x, \
        'Some vets are not shown.'

