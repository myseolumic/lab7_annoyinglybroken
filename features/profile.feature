Feature: Changing profile, adding a pet and booking a visit to vet

  Scenario: Updating data

    Given a registered user
    When user adds new First name, Last name, Email
    Then data is updated


  Scenario: Adding a pet with a name consisting of symbols

    Given a registered user
    When user adds a new pet with a ! and ? in the name
    Then user is informed that allowed pet name can contain only letters and whitespaces

  Scenario: Booking a visit

    Given a registered user with a pet dog
    When user books a visit for the pet
    Then user is redirected to /vets page