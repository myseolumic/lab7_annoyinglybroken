Feature: Booking a vet

  Scenario: Picking a vet

    Given there are vets in database
      | name | age | education |
      | Jane Doe | 43 | Education Facility 1 |
      | John Doe | 37 | Education Facility 2 |
      | Martin Doe | 23 | Education Facility 3 |
    When opening vets page
    Then all 3 vets are visible on the page