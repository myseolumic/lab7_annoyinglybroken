Feature: Registering a visit

  Scenario: Registering a visit for bird with other pets

    Given a registered user with multiple pets and a pet Bird
    When user registers a visit with valid input
    Then the visit is registered

  Scenario: Registering a visit for rodent with other pets

      Given a registered user with multiple pets and a pet Rodent
      When user registers a visit with valid input
      Then the visit is registered

  Scenario: Registering a visit for reptile with other pets

      Given a registered user with multiple pets and a pet Reptile
      When user registers a visit with valid input
      Then the visit is registered

  Scenario: Registering a visit for cat with other pets

    Given a registered user with multiple pets and a pet Cat
    When user registers a visit with valid input
    Then the visit is registered

  Scenario: Registering a visit for dog

    Given a registered user with a pet Dog
    When user registers a visit with valid input
    Then the visit is registered

  Scenario: Registering a visit for cat

    Given a registered user with a pet Cat
    When user registers a visit with valid input
    Then the visit is registered

  Scenario: Registering a visit for bird

    Given a registered user with a pet Bird
    When user registers a visit with valid input
    Then the visit is registered

  Scenario: Registering a visit for rodent

    Given a registered user with a pet Rodent
    When user registers a visit with valid input
    Then the visit is registered

  Scenario: Registering a visit for reptile

    Given a registered user with a pet Reptile
    When user registers a visit with valid input
    Then the visit is registered